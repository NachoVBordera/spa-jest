import { LitElement, html } from "lit";
import { AllEntriesUseCase } from "../usecases/all-entries.usecase";
import "../ui/entry.ui";

export class EntriesComponent extends LitElement {
  static get properties() {
    return {
      entries: { type: Array },
      response: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    await AllEntriesUseCase.execute().then((response) => {
      const entries = new CustomEvent("sit:entries", {
        bubbles: true,
        composed: true,
        detail: {
          response,
        },
      });
      this.dispatchEvent(entries);
      this.entries = response;
    });

    window.addEventListener(
      "sit:entryUpdate",
      (e) => (this.entries = e.detail.response)
    );
  }

  render() {
    return html`
      <section class="sectionEntriesList">
        <button @click="${this.hadlerMode}">ADD</button>
        <ul class="entryList">
          ${this.entries?.map(
            (entry) =>
              html`
                <li @click=${(e) => this.clickEntry(e, entry, this.entries)}>
                  <entry-ui .entry="${entry}"></entry-ui>
                </li>
              `
          )}
        </ul>
      </section>
    `;
  }

  hadlerMode(e) {
    e.preventDefault();
    const message = new CustomEvent("sit:Mode", {
      bubbles: true,
      composed: true,
      detail: {
        addMode: true,
      },
    });
    this.dispatchEvent(message);
  }

  clickEntry(e, currentEntry, entries) {
    e.preventDefault();
    const message = new CustomEvent("sit:Entry", {
      bubbles: true,
      composed: true,
      detail: {
        currentEntry,
        entries,
        addMode: false,
      },
    });

    this.dispatchEvent(message);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("entries-com", EntriesComponent);
