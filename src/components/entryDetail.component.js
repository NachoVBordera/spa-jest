import { LitElement, html } from "lit";
import { PostEntryUseCase } from "../usecases/post-entry.usecase";
import { DeleteEntryUseCase } from "../usecases/delete-entry.usercase";
import { PutEntryUseCase } from "../usecases/put-entry.usecase";

export class EntryDetail extends LitElement {
  static get properties() {
    return {
      currentEntry: { type: Object },
      entries: { type: Array },
      addMode: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.currentEntry = { title: "", content: "" };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.addMode = true;
    window.addEventListener(
      "sit:entries",
      (e) => (this.entries = e.detail.response)
    );
    window.addEventListener("sit:Entry", (e) => {
      this.entries = e.detail.entries;
      this.addMode = e.detail.addMode;
      this.currentEntry = e.detail.currentEntry;
    });
    window.addEventListener("sit:Mode", (e) => {
      this.addMode = true;
      this.handlerResetform(e);
    });
    console.log();
  }

  render() {
    return html`
      <section class="sectionForm">
        ${this.addMode
          ? html``
          : html` <article class="currentEntry">
              <h2>${this.currentEntry.title}</h2>
              <p>${this.currentEntry.content}</p>
            </article>`}
        <form id="formEntry">
          <label class="formLabel">
            Title
            <input
              class="formInput"
              type="text"
              @change="${(e) => this.handleChange(e)}"
              value="${this.currentEntry?.title ?? ""}"
            />
          </label>
          <label class="formLabel">
            Body
            <input
              class="formInput"
              type="text"
              @change="${(e) => this.handleChange(e)}"
              value="${this.currentEntry?.content ?? ""}"
            />
          </label>
        </form>
        <span class="spanButtons">
          ${this.addMode
            ? html`<button @click="${(e) => this.handerPost(e)}">Add</button>`
            : html`<button @click="${(e) => this.handlerDelete(e)}">
                  Delete
                </button>
                <button @click="${(e) => this.handlerPut(e)}">Update</button>`}
          <button @click="${(e) => this.handlerResetform(e)}">Cancel</button>
        </span>
      </section>
    `;
  }

  handlerResetform(e) {
    e.preventDefault();
    this.renderRoot.querySelector("#formEntry").reset();
    this.currentEntry = {};
    this.addMode = true;
  }

  async handlerDelete(e) {
    e.preventDefault();
    const response = await DeleteEntryUseCase.execute(
      this.currentEntry.id,
      this.entries
    );

    const entryDeleted = new CustomEvent("sit:entryUpdate", {
      bubbles: true,
      composed: true,
      detail: {
        response,
      },
    });
    this.dispatchEvent(entryDeleted);
    this.handlerResetform(e);
  }

  handleChange(e) {
    e.preventDefault();
    this.currentEntry.title = e.target.form[0].value;
    this.currentEntry.content = e.target.form[1].value;
  }

  async handerPost(e) {
    e.preventDefault();

    const response = await PostEntryUseCase.execute(
      this.currentEntry,
      this.entries
    );

    const entryAdded = new CustomEvent("sit:entryUpdate", {
      bubbles: true,
      composed: true,
      detail: {
        response,
      },
    });
    this.dispatchEvent(entryAdded);
    this.handlerResetform(e);
  }

  async handlerPut(e) {
    e.preventDefault();
    const response = await PutEntryUseCase.execute(
      this.currentEntry,
      this.entries
    );
    const entryUpdate = new CustomEvent("sit:entryUpdate", {
      bubbles: true,
      composed: true,
      detail: {
        response,
      },
    });
    this.dispatchEvent(entryUpdate);
    this.handlerResetform(e);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("entry-detail", EntryDetail);
