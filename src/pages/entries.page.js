import "../components/entries.component";
import "../components/entryDetail.component";
export class EntriesPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <section class="ContainerPage">
    <entries-com class="entriesContainer"></entries-com>
    <entry-detail class="formContainer"></entry-detail>
    </section>
    `;
  }
}

customElements.define("entries-page", EntriesPage);
