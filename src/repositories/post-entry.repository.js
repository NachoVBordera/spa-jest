import axios from "axios";

export class PostEntriesRepository {
  async postEntry(entry) {
    const entryDtl = {
      title: entry.title,
      body: entry.content,
    };
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", entryDtl)
    ).data;
  }
}
