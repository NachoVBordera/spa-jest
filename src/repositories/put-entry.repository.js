import axios from "axios";

export class PutEntryRepository {
  async putEntry({ id, title, content: body }) {
    //console.log("[PUT ENTRY REPOSITORY]", id, title, body);
    return await (
      await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        id,
        title,
        body,
      })
    ).data;
  }
}
