import axios from "axios";

export class DeleteEntryRepository {
  async deleteEntry(id) {
    return await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
  }
}
