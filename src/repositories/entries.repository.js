import axios from "axios";

export class EntriesRepository {
  async getAllEntries() {
    return await (
      await axios.get("https://jsonplaceholder.typicode.com/posts")
    ).data;
  }
}
