import { PutEntryRepository } from "../repositories/put-entry.repository";

export class PutEntryUseCase {
  static async execute(entry, entriesList = []) {
    const repository = new PutEntryRepository();
    //console.log("[PUT ENTRY USE CASE]", entry, entriesList);
    const uptadetEntry = await repository.putEntry(entry);
    const modelEntryUpdated = {
      id: uptadetEntry.id,
      title: uptadetEntry.title,
      content: uptadetEntry.body,
    };
    return entriesList.map((entry) =>
      entry.id === modelEntryUpdated.id
        ? (entry = modelEntryUpdated)
        : (entry = entry)
    );
  }
}
