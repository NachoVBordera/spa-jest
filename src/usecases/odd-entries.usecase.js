export class OddEntriesUseCase {
  static async execute(entries) {
    return entries.filter((entry) => entry.id % 2 !== 0);
  }
}
