import { Entry } from "../model/entry";
import { EntriesRepository } from "../repositories/entries.repository";

export class AllEntriesUseCase {
  static async execute() {
    const repository = new EntriesRepository();
    const entries = await repository.getAllEntries();
    return entries.map(
      (entrie) =>
        new Entry({
          id: entrie.id,
          title: entrie.title,
          content: entrie.body,
        })
    );
  }
}
