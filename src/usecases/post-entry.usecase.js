import { Entry } from "../model/entry";
import { PostEntriesRepository } from "../repositories/post-entry.repository";

export class PostEntryUseCase {
  static async execute(entry, entrieslist) {
    const repository = new PostEntriesRepository();
    const entryApi = await repository.postEntry(entry);
    const newEntryModel = new Entry({
      id: entryApi.id,
      title: entryApi.title,
      content: entryApi.body,
    });
    return [newEntryModel, ...entrieslist];
  }
}
