import { DeleteEntryRepository } from "../repositories/delete-entry.repository";

export class DeleteEntryUseCase {
  static async execute(entryId, entries) {
    const repository = new DeleteEntryRepository();
    const entriesmodification = await repository.deleteEntry(entryId);
    return entries.filter((entry) => entry.id !== entryId);
  }
}
