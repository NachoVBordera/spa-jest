import { LitElement, html } from "lit";

export class EntryUI extends LitElement {
  static get properties() {
    return {
      entry: { type: Object },
    };
  }

  render() {
    return html`
      <article class="entryArticle">
        <h3 class="entryTitle">${this.entry.title}</h3>
      </article>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("entry-ui", EntryUI);
