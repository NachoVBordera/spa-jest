export class Entry {
  constructor({ id, title, content }) {
    this.id = id;
    this.title = title;
    this.content = content;
  }
}
