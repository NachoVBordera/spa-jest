describe("Entry Management", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8080/");
    cy.get('[href="/posts"]').click();
  });

  it("should create an entry", () => {
    const title = "Test";
    cy.get(":nth-child(1) > .formInput").type(title);
    cy.get(":nth-child(2) > .formInput").type(title);
    cy.get(".spanButtons > :nth-child(1)").click();
    cy.get(`:nth-child(1) > entry-ui > .entryArticle > .entryTitle`).should(
      "have.text",
      title
    );
  });

  it("should delete an entry", () => {
    cy.get(`:nth-child(1) > entry-ui > .entryArticle > .entryTitle`).click();
    cy.get(".spanButtons > :nth-child(2)").click();
    cy.get(`:nth-child(1) > entry-ui > .entryArticle > .entryTitle`).should(
      "not.have.text",
      "Test"
    );
  });

  it("should update an entry", () => {
    const newTitle = "New Test";
    cy.get(":nth-child(2) > entry-ui > .entryArticle > .entryTitle").click();
    cy.get(":nth-child(1) > .formInput").clear().type(newTitle);
    cy.get(".spanButtons > :nth-child(2)").click();
    cy.get(":nth-child(2) > entry-ui > .entryArticle > .entryTitle").should(
      "have.text",
      newTitle
    );
  });
});
