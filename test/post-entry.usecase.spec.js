import { PostEntriesRepository } from "../src/repositories/post-entry.repository";
import { PostEntryUseCase } from "../src/usecases/post-entry.usecase";
import { RESPONSE } from "./fixtures/post-response";
import { Entry } from "../src/model/entry";

const entry = new Entry({
  title: "new title",
  content: "new body text",
});

jest.mock("../src/repositories/post-entry.repository");
describe("POST entry use case", () => {
  beforeEach(() => {
    PostEntriesRepository.mockClear();
  });

  it("should post new entry", async () => {
    PostEntriesRepository.mockImplementation(() => {
      return {
        postEntry: () => {
          return {
            id: 101,
            title: entry.title,
            body: entry.content,
            userId: 1,
          };
        },
      };
    });
    const ENTRIES = [
      {
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        id: 2,
        title: "qui est esse",
        content:
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
      {
        userId: 1,
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        content:
          "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
      },
    ];

    const response = await PostEntryUseCase.execute(entry, ENTRIES);

    //console.log("[RESPONSE]: ", response.data.body, "[Entry]: ", entry);

    expect(response.length).toBe(ENTRIES.length + 1);
    expect(response[0].title).toBe(entry.title);
    expect(response[0].content).toBe(entry.content);
  });
});
