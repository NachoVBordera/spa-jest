import { Entry } from "../src/model/entry";
import { PutEntryRepository } from "../src/repositories/put-entry.repository";
import { PutEntryUseCase } from "../src/usecases/put-entry.usecase";
import { RESPONSE } from "./fixtures/put-response";

jest.mock("../src/repositories/put-entry.repository");
describe("Edit an entry use case", () => {
  const entry = new Entry({
    id: 1,
    title: "title edited",
    content: "content edited",
  });

  beforeEach(() => {
    PutEntryRepository.mockClear();
  });
  it("Should edit an entry", async () => {
    const ENTRIES = [
      {
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        id: 2,
        title: "qui est esse",
        content:
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
      {
        userId: 1,
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        content:
          "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
      },
    ];
    PutEntryRepository.mockImplementation(() => {
      return {
        putEntry: () => {
          return {
            id: entry.id,
            title: entry.title,
            body: entry.content,
            userId: 1,
          };
        },
      };
    });

    const response = await PutEntryUseCase.execute(entry, ENTRIES);

    //console.log("[Response]: ", response[0], "[entry]: ", entry);
    expect(response.length).toBe(3);
    expect(response[0].title).toBe(entry.title);
    expect(response[0].content).toBe(entry.content);
  });
});
