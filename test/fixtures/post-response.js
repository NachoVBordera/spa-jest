export const RESPONSE = {
  status: 201,
  data: {
    body: { id: 12, title: "test", content: "body text" },
    headers: { "Content-type": "application/json; charset=UTF-8" },
    id: 101,
  },
};
