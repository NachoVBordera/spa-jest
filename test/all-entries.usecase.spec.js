import { ENTRIES } from "./fixtures/entries";
import { EntriesRepository } from "../src/repositories/entries.repository";
import { AllEntriesUseCase } from "../src/usecases/all-entries.usecase";

jest.mock("../src/repositories/entries.repository");

describe("All entries use case", () => {
  beforeEach(() => {
    EntriesRepository.mockClear();
  });

  it("should get all entries", async () => {
    EntriesRepository.mockImplementation(() => {
      return {
        getAllEntries: () => {
          return ENTRIES;
        },
      };
    });

    const entries = await AllEntriesUseCase.execute();

    expect(entries.length).toBe(100);

    expect(entries[0].title).toBe(ENTRIES[0].title);
    expect(entries[0].content).toBe(ENTRIES[0].body);
  });
});
