import { DeleteEntryRepository } from "../src/repositories/delete-entry.repository";
import { DeleteEntryUseCase } from "../src/usecases/delete-entry.usercase";
import { RESPONSE } from "./fixtures/delete-respnse";

jest.mock("../src/repositories/delete-entry.repository");
describe("Delete an entry use case", () => {
  beforeEach(() => {
    DeleteEntryRepository.mockClear();
  });

  it("Should delete an entry", async () => {
    DeleteEntryRepository.mockImplementation(() => {
      return {
        deleteEntry: () => {},
      };
    });

    const ENTRIES = [
      {
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        id: 2,
        title: "qui est esse",
        content:
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
      {
        userId: 1,
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        content:
          "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
      },
    ];

    const entryId = 1;
    const response = await DeleteEntryUseCase.execute(entryId, ENTRIES);

    //console.log(response.data);

    expect(response.length).toBe(ENTRIES.length - 1);
    expect(response[0].id).toBe(2);
  });
});
